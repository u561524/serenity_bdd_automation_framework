package starter;

//import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

//import cucumber.api.CucumberOptions;

import io.cucumber.junit.CucumberOptions;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features/",
        glue = {"starter.stepdefinitions"},
     //   tags = {"@Issue:PHO-3326"},
       // plugin = {"pretty","html:test-outout","json:json_output/cucumber.json","junit:junit_xml/cucumber.xml"},
       // monochrome = true,
      //  strict = true,
        dryRun = false        
)
public class CucumberTestSuite {}
