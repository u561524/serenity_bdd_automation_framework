package starter.stepdefinitions;

import birdy_PageFiles.dashboardpage;
import birdy_PageFiles.loginpage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class createNewDemandinBridy {
	
	@Steps
	loginpage loginwebelements;
	
	@Steps
	dashboardpage dashboardwebelements;
	
	@Given("user login with valid username {string} and password {string}")
	public void user_login_with_valid_username_and_password(String username, String password) 
	{
		loginwebelements.launchbirdyapplication();
	    loginwebelements.enterusername(username);
	    loginwebelements.enterpassword(password);
	    loginwebelements.clickonloginbutton();
	}
	
	@Given("user selects the postalLocation")
	public void user_selects_the_postalLocation() 
	{
		dashboardwebelements.selectPostalLocation();
	}

	@When("user Clicks on ProcessDemand to create New Damand")
	public void user_Clicks_on_ProcessDemand_to_create_New_Damand() 
	{
	    dashboardwebelements.clickonProcessDemandbutton();
	}

	@When("user enters valid Barcode value {string}")
	public void user_enters_valid_Barcode_value(String barcodevalue) 
	{
	    dashboardwebelements.entervalidbarcode(barcodevalue);
	}
	

	@When("user clicks on search button")
	public void user_clicks_on_search_button() 
	{
	    dashboardwebelements.searchbarcodevalue();
	}

	@When("user enters address {string} and search")
	public void user_enters_address_and_search(String addressvalue) 
	{
	    dashboardwebelements.enteraddress(addressvalue);
	    
	    
	}
	
	@When("user search address")
	public void user_search_address() 
	{
		dashboardwebelements.searchaddressval();
	}

	@Then("user selects NB Radiobutton")
	public void user_selects_NB_Radiobutton() 
	{
	   dashboardwebelements.selectNBradiobutton();
	}

	@Then("user clicks on CreateDemand button")
	public void user_clicks_on_CreateDemand_button() 
	{
	    dashboardwebelements.clickonCreatebutton();
	}

	@Then("New Demand Created successfully")
	public void new_Demand_Created_successfully() 
	{
	   dashboardwebelements.verifyNewDemandcreated();
	}

}
