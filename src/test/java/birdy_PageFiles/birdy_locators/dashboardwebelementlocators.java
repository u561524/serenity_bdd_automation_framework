package birdy_PageFiles.birdy_locators;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Managed;

public class dashboardwebelementlocators extends PageObject {
	
	@Managed                                                            
    WebDriver driver;
	
	public void selectpostalloc()
	{
		
		$(By.xpath("//a[contains(@href,'select-postallocation')]")).click();
		$(By.xpath("//div[contains(@class, 'ui-select-match')]")).click();
		//$(By.id("ui-select-choices-row-1-0")).click();
		$(By.xpath("(//*[contains(@class, 'ui-select-choices-row-inner')])[1]")).click();
	
	}
	
	public void clickonDemandProcessbtn() 
	{
		$(By.xpath("//a[contains(text(),'Process demand')]")).click();
	}
	
	public void enterbarcode(String barcodevalue) 
	{
		$(By.xpath("//*[contains(@name,'searchBarcode')]")).type(barcodevalue);
	}
	
	public void clickonSearchbarcodebtn()
	{
		$(By.xpath("//span[contains(text(),'Search')]")).click();
	}
	
	public void selectradiobtn()
	{
		$(By.xpath("//input[@value='WITHOUT_PLANNING']")).click();
		$(By.id("handlingModeNB")).click();
	}
	
	public void enteraddressvalue(String addressvalue)
	{
		try
			{
				Robot robot = new Robot();
				$(By.id("addressInputField")).sendKeys(addressvalue);
				robot.delay(3000);
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);
				robot.delay(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				//robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
				//robot.delay(3000);// press left click	
				//robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
				robot.delay(3000);
			}
		catch (AWTException ae) 
			{
		    ae.printStackTrace();
			}
				
	}
	
	public void searchaddressvalue()
	{
		System.out.println("Address has been validated successfully");

		/*
		 * //$(By.xpath("//*[@id=\"bpaac-suggestionsList\"]/li[1]")).click();
		 * $(By.xpath("//button[contains(@class, 'bpaac-button')]")).click();
		 * 
		 * WebElement select = $(By.xpath("(//*[@id='bpaac-suggestionsList'])[1]"));
		 * 
		 * List<WebElement> options = select.findElements(By.tagName("li"));
		 * 
		 * for (WebElement option1 : options) {
		 * 
		 * if("ABDIJSTRAAT 1  -  1000 BRUSSEL".equals(option1.getText().trim()))
		 * 
		 * option1.click(); }
		 */     		
		
	}
	
	public void clickoncreateDemand()
	{
		$(By.xpath("(//button[@type='submit' and contains(., \"Create\")])[2]")).click();
	}
	
	public void demandCreadtedsuccessfully() 
	{
		$(By.xpath("//strong[contains(text(),'has been created.')]")).getText();
		
	}


}
