package birdy_PageFiles.birdy_locators;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;

public class loginwebelementlocators extends PageObject {
	
	public void loginpagetitle()
	{
		System.out.println(getDriver().getTitle());
	}
	
	public void enterUsrname(String usrname)
	
	{
		$(By.id("username")).type(usrname);
	}
	
	public void enterpaswrd(String paswrd)
	
	{
		$(By.id("password")).type(paswrd);
	}
	
	public void clickloginbtn()
	
	{
		
		$(By.xpath("//button[@type='submit' and span='Login']")).click();
	}


}
