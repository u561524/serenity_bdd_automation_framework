package birdy_PageFiles;

import birdy_PageFiles.birdy_locators.dashboardwebelementlocators;
import net.thucydides.core.annotations.Step;

public class dashboardpage {
	
	dashboardwebelementlocators dashboardlocators;
	
	@Step
	public void selectPostalLocation()
	{
		dashboardlocators.selectpostalloc();
	}
	
	@Step
	public void createNewDemand(String barcodevalue,String addressvalue) 
	{
		
		dashboardlocators.enterbarcode(barcodevalue);
		dashboardlocators.clickonSearchbarcodebtn();
		dashboardlocators.selectradiobtn();
		dashboardlocators.enteraddressvalue(addressvalue);
		dashboardlocators.searchaddressvalue();
		dashboardlocators.clickoncreateDemand();
		dashboardlocators.demandCreadtedsuccessfully();
	}
	
	@Step
	public void clickonProcessDemandbutton()
	
	{
		dashboardlocators.clickonDemandProcessbtn();
	}
	
	@Step
	public void entervalidbarcode(String barcodevalue)
	{
		dashboardlocators.enterbarcode(barcodevalue);
		
	}
	
	@Step
	public void searchbarcodevalue()
	{
		dashboardlocators.clickonSearchbarcodebtn();
	}
	
	@Step
	public void selectNBradiobutton()
	{
		dashboardlocators.selectradiobtn();
	}
	
	@Step
	public void enteraddress(String addressvalue)
	{
		dashboardlocators.enteraddressvalue(addressvalue);
		
	}
	
	@Step
	public void searchaddressval()
	{
		dashboardlocators.searchaddressvalue();
	}
	
	@Step
	public void clickonCreatebutton()
	{
		dashboardlocators.clickoncreateDemand();
	}
	
	@Step
	public void verifyNewDemandcreated()
	{
		dashboardlocators.demandCreadtedsuccessfully();
	}

}
