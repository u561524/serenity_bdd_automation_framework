package birdy_PageFiles;

import birdy_PageFiles.birdy_locators.loginwebelementlocators;
import net.thucydides.core.annotations.Step;

public class loginpage {
	
	loginwebelementlocators loginlocators;
	
	@Step
	public void launchbirdyapplication()
	{
		loginlocators.open();
	}
	
	
	@Step
	public void enterusername(String username)
	{
		loginlocators.enterUsrname(username);
	}
	
	@Step
	public void enterpassword(String password)
	{
		loginlocators.enterpaswrd(password);
	}
	
	@Step
	public void clickonloginbutton()
	{
		loginlocators.clickloginbtn();
	}
	

}
