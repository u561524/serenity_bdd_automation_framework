#Keywords Summary : Login to Birdy application and create New Demand


 Feature: Create New Demand in Birdy application with valid barcode value

@Issue:PHO-3326
 Scenario Outline:
  		Given user login with valid username "<username>" and password "<password>"
  		And user selects the postalLocation
  		When user Clicks on ProcessDemand to create New Damand
  		And user enters valid Barcode value "<barcodevalue>"
  		And user clicks on search button
  		And user enters address "<addressvalue>" and search
  		And user selects NB Radiobutton
  		And user clicks on CreateDemand button
  		Then New Demand Created successfully
  		
  Examples:
  	| username | password    | barcodevalue								| addressvalue |
  	| u561524	 | Wednesday17 | ${!3232125061000004307,r7} | ABDIJSTRAAT 1|
  		