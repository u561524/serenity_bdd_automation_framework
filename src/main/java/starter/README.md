### Application code
 
These packages generally contain application code. 
If you are writing a reusable test library, you can also place reusable test components such as Page Objects or Tasks here.

#Start using Serenity BDD framework frpm the following bitbucket link
https://bitbucket.org/u561524/serenity_bdd_automation_framework/src/master/

#Inorder to run the Serenity Tests - goto serenity bdd framework path and run the command lime below
#run command = mvn clean verify (this command will execute clean maven test) from command prompt
# or Goto src/test/java with starter package we can see CucumberTestSuite class and right click and run maven build
# once the Test execution completed we can see the Test Result Report as index.html