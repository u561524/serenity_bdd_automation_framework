$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/birdy/birdyLogin.feature");
formatter.feature({
  "name": "Birdy Application Login Feature",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@issue:\u003cFeature"
    },
    {
      "name": "ID\u003e"
    }
  ]
});
formatter.scenarioOutline({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@issue:\u003cOrder"
    },
    {
      "name": "\u003d"
    },
    {
      "name": "1\u003e"
    }
  ]
});
formatter.step({
  "name": "user is on the login Page",
  "keyword": "Given "
});
formatter.step({
  "name": "title of login page is Birdy",
  "keyword": "When "
});
formatter.step({
  "name": "user enters \"\u003cusername\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User clicks on login button",
  "keyword": "Then "
});
formatter.step({
  "name": "user is on home page",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "u561524",
        "Wednesday17"
      ]
    }
  ]
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@issue:\u003cFeature"
    },
    {
      "name": "ID\u003e"
    },
    {
      "name": "@issue:\u003cOrder"
    },
    {
      "name": "\u003d"
    },
    {
      "name": "1\u003e"
    }
  ]
});
formatter.step({
  "name": "user is on the login Page",
  "keyword": "Given "
});
formatter.match({
  "location": "starter.stepdefinitions.login_birdy.user_is_on_the_login_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "title of login page is Birdy",
  "keyword": "When "
});
formatter.match({
  "location": "starter.stepdefinitions.login_birdy.title_of_login_page_is_Birdy()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters \"u561524\" and \"Wednesday17\"",
  "keyword": "Then "
});
formatter.match({
  "location": "starter.stepdefinitions.login_birdy.user_enters_and(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on login button",
  "keyword": "Then "
});
formatter.match({
  "location": "starter.stepdefinitions.login_birdy.user_clicks_on_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is on home page",
  "keyword": "Then "
});
formatter.match({
  "location": "starter.stepdefinitions.login_birdy.user_is_on_home_page()"
});
formatter.result({
  "status": "passed"
});
});